SFMovie = {
	map : null,
	geocoder : null, 
	overlappingMarkerSpider : null,
	overlappingInfoWindow : null
};

SFMovie.config = {
		searchRadius : 1,
		numberOfResults : 40,
		compressedImageSize : 24,
		mediumCompressedImageSize : 32
};

SFMovie.centerMarker = {};

SFMovie.singleLocationIcon = {
				url: 'img/movie.png',
    	    	size: new google.maps.Size(SFMovie.config.compressedImageSize, SFMovie.config.compressedImageSize),
    	    	scaledSize: new google.maps.Size(SFMovie.config.compressedImageSize, SFMovie.config.compressedImageSize)
};

SFMovie.multiLocationIcon = {
				url: 'img/hotmovie.png',
    	    	size: new google.maps.Size(SFMovie.config.compressedImageSize, SFMovie.config.compressedImageSize),
    	    	scaledSize: new google.maps.Size(SFMovie.config.compressedImageSize, SFMovie.config.compressedImageSize)
};

SFMovie.markers = [];
SFMovie.movies = [];
SFMovie.filters = {
	"title" : {value: null, label: "Title"},
	"releaseYear" :{value: null, label: "Release Year"},
	"director" : {value: null, label: "Director"},
	"actors" : {value: null, label: "Actor"},
	"productionCompany" : {value: null, label: "Production Company"}
};


SFMovie.SHOW_NEARBY_MOVIES = 'movies/nearby'
SFMovie.SHOW_ALL_LOCATIONS = 'locations'

//initialize google maps, center marker, and spider markers
SFMovie.initGoogleMaps = function(latitude, longitude) {
	SFMovie.map = new google.maps.Map(document.getElementById("map-canvas"), {
							center : new google.maps.LatLng(latitude,longitude),
	      					zoom: 16});

	SFMovie.centerMarker = new google.maps.Marker({
        			map:SFMovie.map,
        			draggable:false,
        			animation: google.maps.Animation.DROP,
        			title: "My Location",
        			position: new google.maps.LatLng(latitude,longitude),
        			icon: {
							url: 'img/pin.png',
    	    				size: new google.maps.Size(SFMovie.config.mediumCompressedImageSize, SFMovie.config.mediumCompressedImageSize),
    	    				scaledSize: new google.maps.Size(SFMovie.config.mediumCompressedImageSize, SFMovie.config.mediumCompressedImageSize)
					}
	});

	SFMovie.markers.push(SFMovie.centerMarker);

	SFMovie.geocoder = new google.maps.Geocoder();

	SFMovie.overlappingMarkerSpider = new OverlappingMarkerSpiderfier(SFMovie.map, {markersWontMove: true, markersWontHide: true});

	SFMovie.overlappingInfoWindow = new google.maps.InfoWindow();

	SFMovie.overlappingMarkerSpider.addListener('click', function(marker, event) {
		SFMovie.overlappingInfoWindow.setContent(marker.desc);
  		SFMovie.overlappingInfoWindow.open(SFMovie.map, marker);
	});

	SFMovie.overlappingMarkerSpider.addListener('spiderfy', function(markers) {
  		$.each(markers, function(index,marker){
  			marker.setIcon(SFMovie.singleLocationIcon);
  		});

  		SFMovie.overlappingInfoWindow.close();
	});

	SFMovie.overlappingMarkerSpider.addListener('unspiderfy', function(markers) {
  		$.each(markers, function(index,marker){
  			marker.setIcon(SFMovie.multiLocationIcon);
  		});
	});
};


SFMovie.createMarker = function(movie, isSingleLocation) {
	var position = new google.maps.LatLng(movie.latitude, movie.longitude);

	var icon = isSingleLocation ? SFMovie.singleLocationIcon : SFMovie.multiLocationIcon

	var marker = new google.maps.Marker({
        map:SFMovie.map,
        draggable:false,
        animation: google.maps.Animation.DROP,
        position: position,
        title: movie.title,
        icon: icon,
        desc:  _.template($('#movie-info-window-template').html(), movie)
     });

	return marker;
};

// Removes the markers from the map and clears the marker array.
SFMovie.clearMarkers = function() {
   $.each(SFMovie.markers, function(index, marker) {
   		//always keep the cetner marker
   		if (index>0) {
   			marker.setMap(null);
   		}   		
   });
   SFMovie.markers = [SFMovie.centerMarker];
};

/**Twitter typeahead javascript**/
function loadTypeAheadComponents(el) {
	var locations = new Bloodhound({
		  datumTokenizer: function(d) {return Bloodhound.tokenizers.whitespace(d.location.replace(/[^\w\s]|_/g, " "));},
		  queryTokenizer: Bloodhound.tokenizers.whitespace,
		  prefetch: {
			  	ttl: 1,
		        url: SFMovie.SHOW_ALL_LOCATIONS,
		        filter: function (list) {
		            return $.map(list, function (movie) {
		            	return {location: movie.locations, lat: movie.latitude, lng: movie.longitude};});
		        }
		    }
	});
	
	locations.initialize();	
	el.typeahead({
		hint: true,
		highlight: true,
	    minLength: 1
	},
	{
		name: 'locations',
		displayKey: 'location',
		source: locations.ttAdapter()
	}).on('typeahead:selected', onSelected);

	el.keypress(onEnter);
}

//handle user search
function onEnter(event) {
	//each time the user hits enter, send a search nearby request
	if (event.keyCode===13) {
		var address = event.target.value;
		SFMovie.geocoder.geocode({'address': address}, function(results, status) {
  			if (status == google.maps.GeocoderStatus.OK && results.length>0) { 		
  				nearbyMovieView.render({latitude:results[0].geometry.location.k, longitude:results[0].geometry.location.B});
			} else {
				alert("No matching address!");
			}
		});
	}
}

//handle selection of the autocompletion
function onSelected(event, data) {
	nearbyMovieView.render({latitude:data.lat, longitude:data.lng, shift:true});
}

/**  BACKBONE JAVASCRIPT BELOW  **/
var NearbyMovie = Backbone.Model.extend({
	defaults: {
            latitude: null,
            longitude: null
    },
	url : function() {
		console.log("SENDING REQUEST - " + SFMovie.SHOW_NEARBY_MOVIES + '/'+this.get('longitude')+'/'+this.get('latitude')
				+ '?radius='+SFMovie.config.searchRadius+'&numberOfResults='+SFMovie.config.numberOfResults);
		return SFMovie.SHOW_NEARBY_MOVIES+'/'+this.get('longitude')+'/'+this.get('latitude') 
				+ '?radius='+SFMovie.config.searchRadius+'&numberOfResults='+SFMovie.config.numberOfResults;
	}
});


var NearbyMovieView = Backbone.View.extend({
	el: '.container .filter',
	events: {
		'click a.releaseYearOption' : 'filterMovies',
		'click a.productionCompanyOption' : 'filterMovies',
		'click a.directorOption' : 'filterMovies',
		'click a.titleOption' : 'filterMovies',
		'click a.actorsOption' : 'filterMovies'
	},
	filterMovies : function(ev) {
		var filterKey = $(ev.currentTarget).data('filter-key');
		var filterValue = $(ev.currentTarget).data('filter-value');

		var text = filterValue;
		//if selects all, set back to filter label
		if (!filterValue) {
			text = SFMovie.filters[filterKey].label;
		}

		//set the dropdown text
		$(".filter #"+filterKey+"DropdownMenu.btn:first-child").html(text);
	    $(".filter #"+filterKey+"DropdownMenu.btn:first-child").val(text);

	    if (filterValue) {
	    	SFMovie.filters[filterKey].value = filterValue;
	    } else {
	    	SFMovie.filters[filterKey].value = null;
	    }	    

	    //apply filter selections
	    var movies = $.grep(SFMovie.movies, function(movie, index){
	    	var match = true;
	    	
	    	$.each(SFMovie.filters, function(key, value) {
	    		if (value.value!=null) {
	    			if (key==='actors') {
	    				match = (match && _.indexOf(movie[key], value.value, false)!=-1);
	    			} else {
	    				match = (match && value.value===movie[key]);
	    			}	    			
	    		}	    		
	    	});

  			return match;
		});

	    SFMovie.clearMarkers();
	    SFMovie.overlappingMarkerSpider.clearMarkers();
	    var groupedMovies = _.groupBy(movies, function(movie) {
    		return movie.latitude + "-" + movie.longitude;
 		});

	    //use the spider plugin to group by closet marker, so when you click it, it will expand
 		$.each(groupedMovies, function(key, value){
 			var singleLocationIcon = true;
 			if (value.length>1) {
 				singleLocationIcon = false;
 			}

 			$.each(value, function(index, movie) {
				var marker = SFMovie.createMarker(movie, singleLocationIcon);
				SFMovie.markers.push(marker);
				SFMovie.overlappingMarkerSpider.addMarker(marker);
			});

 		});
	},
	render: function(options) {
		var that = this;
		var nearbyMovie = new NearbyMovie(options);
		nearbyMovie.fetch().then(function(response){
			if (response.length>0) {
				SFMovie.clearMarkers();
				//clear the filters
				SFMovie.movies = [];
				SFMovie.overlappingMarkerSpider.clearMarkers();

				var latitude = options.latitude;
				var longitude = options.longitude;
				//in some case, do not overlap the movie location with center location
				if (options.shift) {
					latitude = latitude - 0.0003;
					longitude = longitude - 0.0003;
				}
 
    		 	SFMovie.centerMarker.setPosition(new google.maps.LatLng(latitude, longitude));
    		 	SFMovie.map.setCenter(new google.maps.LatLng(latitude, longitude));


    		 	var groupedMovies = _.groupBy(response, function(movie) {
    				return movie.latitude + "-" + movie.longitude;
 				});

 				$.each(groupedMovies, function(key, value){
 					var singleLocationIcon = true;
 					if (value.length>1) {
 						singleLocationIcon = false;
 					}

 					$.each(value, function(index, movie) {
						var marker = SFMovie.createMarker(movie, singleLocationIcon);
						SFMovie.markers.push(marker);
						SFMovie.overlappingMarkerSpider.addMarker(marker);
					});

 				});

          	 	SFMovie.movies = response;

          	 	var filterTemplate = "";
			 
			 	/** create filters **/
			 	$.each(SFMovie.filters, function(key, value){
			 		value.value = null;
			 		var data = [];

			 		if (key==='actors') {
			 			data = _.uniq(_.sortBy(_.union.apply(_,_.map(SFMovie.movies, function(movie) { 
    							return movie[key];
			 				}))), true);
			 		} else {
			 			data = _.uniq(_.sortBy(_.map(response, function(movie) { 
    						return movie[key];
			 			})), true);
			 		}

			 		filterTemplate += _.template($('#dropdown-template').html(),
					{
						label : value.label,
						key : key,
						items : data
					});
			 	});
			 
			 	$('.filter').html(filterTemplate);
			 } else {
			 	alert("No movies are filmed nearby!");
			 }

          }, function(response){
				console.log(response);
				alert('Request failed');
          });
	}
});

var SearchMovieView = Backbone.View.extend({
	el: '.container .movie-search-form #movie-search',
	render: function(options) {
		var that = this;
		loadTypeAheadComponents(that.$el);
	}
});


var nearbyMovieView = new NearbyMovieView();
var searchMovieView = new SearchMovieView();

//init method to start the app
SFMovie.initSFMovie = function() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(SFMovie.initLocation);
    } else { 
        alert("Geolocation is not supported by this browser.");
    }
};

//callback after getting browser location
SFMovie.initLocation = function(position) {
	SFMovie.initGoogleMaps(position.coords.latitude,position.coords.longitude);
    searchMovieView.render();
	nearbyMovieView.render({latitude: position.coords.latitude, longitude: position.coords.longitude});
};

var Router = Backbone.Router.extend({
    routes: {
      "" : "home"
    }
});

var router = new Router();
router.on("route:home", function() {
	SFMovie.initSFMovie();
}); 
Backbone.history.start();



//a hack to allow google map to display in boostrap3 
$(window).resize(function () {
    var h = $(window).height();
    var offsetTop = 20; // Calculate the top offset
    $('#map-canvas').css('height', (h - offsetTop));
}).resize();
