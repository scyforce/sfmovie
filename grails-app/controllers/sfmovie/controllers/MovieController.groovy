package sfmovie.controllers

import groovy.json.*
import sfmovie.controllers.beans.Movie

//This controller serves all movie requests from client
class MovieController {
	def movieService
	def errorService
	
	def final DEFAULT_NUMBER_OF_RESULTS = 40
	def final DEFAULT_RADIUS = 1.0
	def final ONE_MILE = 1.60934
	
	def getAllMovies() {
		try {
			def allMovies = movieService.getAllMovies()
			def builder = new JsonBuilder();
			def root = builder(
				allMovies.collect { movie ->
					[
						"releaseYear" : movie.releaseYear,
						"title":movie.title,
						"productionCompany":movie.productionCompany,
						"director":movie.director,
						"writer":movie.writer,
						"actors":movie.actors,
						"longitude":movie.longitude,
						"latitude":movie.latitude,
						"funFacts":movie.funFacts,
						"distributor":movie.distributor,
						"locations":movie.location
					]}
			)
			
			response.status = 200
			response.setContentType("application/json")
			render JsonOutput.prettyPrint(builder.toString())
		} catch(Exception e) {
			log.error "Exception getAllLocations.getAllMovies() ${e}"
			render JsonOutput.prettyPrint(errorService.renderStatus(response, 500, "Internal error happens."))
		}
	}
	
	def getAllLocations() {
		try {
			def movies = movieService.getAllLocations()
			def builder = new JsonBuilder();
			def root = builder(
				movies.collect { movie ->
					[
						"longitude":movie.longitude,
						"latitude":movie.latitude,
						"locations":movie.location
					]}
			)
			
			response.status = 200
			response.setContentType("application/json")
			render JsonOutput.prettyPrint(builder.toString())
		} catch(Exception e) {
			log.error "Exception MovieController.getAllLocations() ${e}"			
			render JsonOutput.prettyPrint(errorService.renderStatus(response, 500, "Internal error happens."))			
		}
	}
	
    def getNearbyMovies() { 		
		try{
			if (!validateLatLng(params.longitude, params.latitude)) {
				render errorService.renderStatus(response, 400, "Please pass both longitude and latitude")
				return
			}
			
			if (!validateNumberFormat(params.longitude) || !validateNumberFormat(params.latitude)) {
				render errorService.renderStatus(response, 400, "Longitude and latitude should be number")
				return
			}
			
			//numberOfResults and radius are optional parameters
			if (!validateNumberFormat(params.numberOfResults) || !validateNumberFormat(params.radius)) {
				render errorService.renderStatus(response, 400, "radius and numberOfResults should be number")
				return
			}
			
			def numberOfResults = (params.numberOfResults==null ? DEFAULT_NUMBER_OF_RESULTS : params.numberOfResults.toInteger())
			def nearbyMeters = (params.radius==null ? DEFAULT_RADIUS : params.radius.toDouble())
			def nearbyMovies = movieService.getNearbyMovies(params.longitude.toDouble(), params.latitude.toDouble(), numberOfResults, nearbyMeters * ONE_MILE * 1000)
			def builder = new JsonBuilder();
			def root = builder(
				nearbyMovies.collect { nearbyMovie ->
					[
						"id" : nearbyMovie.id,
						"releaseYear" : nearbyMovie.releaseYear,
						"title":nearbyMovie.title,
						"productionCompany":nearbyMovie.productionCompany,
						"director":nearbyMovie.director,
						"writer":nearbyMovie.writer,
						"actors":nearbyMovie.actors,
						"longitude":nearbyMovie.longitude,
						"latitude":nearbyMovie.latitude,
						"funFacts":nearbyMovie.funFacts,
						"distributor":nearbyMovie.distributor,
						"locations":nearbyMovie.location
					]}
			)
			
			response.status = 200
			response.setContentType("application/json")
			render JsonOutput.prettyPrint(builder.toString())
		} catch(Exception e) {
			log.error "Exception MovieController.getNearbyMovies() ${e}"
			render JsonOutput.prettyPrint(errorService.renderStatus(response, 500, "Internal error happens."))
		}
	}
	
	def validateLatLng(longitude, latitude) {
		return longitude!=null && latitude!=null
	}
	
	def validateNumberFormat(String number) {
		return (number!=null) ? number.isNumber() : true;
	}
}
