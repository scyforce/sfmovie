package sfmovie.controllers.beans

class Movie {
	def int id
	def String title
	def int releaseYear
	def double latitude
	def double longitude
	def String funFacts
	def String productionCompany
	def String distributor
	def String director
	def String writer
	def List<String> actors
	def String location
}
