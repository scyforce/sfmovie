package sfmovie.controllers

import groovy.json.*;

//Data initialization controller
class InitializeDataController {

	def sfGovService
	def movieService
	def errorService
	
    def init() { 	
		try {
			def movies = sfGovService.getSfGovData()
			movieService.refreshMovieDatabase(movies)
		} catch (Exception e) {
			log.error "Exception InitializeDataController.init() ${e}"
			render JsonOutput.prettyPrint(errorService.renderStatus(response, 500, "Internal error happens."))
		}
		
	}
}
