package sfmovie.services

//this class is to generate error message 
class ErrorService {

	def renderStatus(response, errorCode, errorMessage) {
		def results = ["responseCode": errorCode, "message": errorMessage];
		response.status = errorCode
		response.setContentType("application/json")
		return results
	}

}
