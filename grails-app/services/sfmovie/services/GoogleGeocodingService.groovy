package sfmovie.services

import groovy.json.JsonSlurper


//This class will send geocoding request to google geocoding api and parse the response 
class GoogleGeocodingService {
	def remoteDataService
	
	def GOOGLE_GEOCODING_URL = 'https://maps.googleapis.com'
	def GOOGLE_GEOCODING_PATH = '/maps/api/geocode/json'
	
	def getGeocodingData(String locations) {
		def query = [address : locations + ', San Francisco']
		
		def geocodingData = remoteDataService.getRemoteData(GOOGLE_GEOCODING_URL, GOOGLE_GEOCODING_PATH, query);
		return parseGeocodingData(geocodingData)
	}
	
	def parseGeocodingData(String data) {
		def slurper = new JsonSlurper()
		def result = slurper.parseText(data)
		
		def coordinates = null
		//sometimes google can not geocode the address, an empty results will be returned
		if (result.results.size()>0) {
			coordinates = [:]
			coordinates.latitude = result.results[0].geometry.location.lat.toDouble()
			coordinates.longitude = result.results[0].geometry.location.lng.toDouble()
		}
		return coordinates
	}
}
