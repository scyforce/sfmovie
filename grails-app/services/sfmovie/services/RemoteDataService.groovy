package sfmovie.services

import groovyx.net.http.*
import static groovyx.net.http.ContentType.*
import static groovyx.net.http.Method.*


//This class is responsible for sending remote http calls
class RemoteDataService {
	
	def getRemoteData(String baseUrl, String path, query, method = Method.GET) throws Exception{
		def ret = null
		def http = new HTTPBuilder(baseUrl)

		// sending http request and get text back
		http.request(method, ContentType.TEXT) {
			uri.path = path
			uri.query = query
			
			// response handler for a success response code
			response.success = { resp, reader ->
				println "response status: ${resp.statusLine}"
				ret = reader.getText()

				println 'Response: -----'
				println ret
				println '--------------------'
			}
		}
		return ret
	}
}
