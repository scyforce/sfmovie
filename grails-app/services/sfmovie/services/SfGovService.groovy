package sfmovie.services

import groovy.json.JsonSlurper

import sfmovie.controllers.beans.Movie

//This class is used to send request to sfgov to get movie locations information
class SfGovService {
	def remoteDataService
	def googleGeocodingService
	
	def SF_MOVIE_DATA_URL = 'http://data.sfgov.org'
	def SF_MOVIE_DATA_PATH = '/resource/yitu-d5am.json'
	
	def getSfGovData() {
		def movieData = remoteDataService.getRemoteData(SF_MOVIE_DATA_URL, SF_MOVIE_DATA_PATH, [:])		
		return parseSfGovData(movieData)
	}
	
    def parseSfGovData(String data) {
		def slurper = new JsonSlurper()
		def sfGovData = slurper.parseText(data)
		
		def movies = []
		def gid = 1
		sfGovData.eachWithIndex {
			obj, index ->
			//some entries does not contain any location information
			if (obj.locations!=null) {
				def coordinates = googleGeocodingService.getGeocodingData(obj.locations)
				//some locations are too vague that google can not geocode, we will skip these
				if (coordinates!=null) {
					Movie movie = new Movie()
					movie.id = gid
					movie.title = obj.title					
					movie.releaseYear = obj.release_year.toInteger()
					movie.productionCompany = obj.production_company
					movie.writer = obj.writer
					movie.director = obj.director
					movie.funFacts = obj.fun_facts
					movie.distributor = obj.distributor
					movie.location = obj.locations					
					movie.actors = [];
					movie.actors.add(obj.actor1);
					movie.actors.add(obj.actor2);
					movie.actors.add(obj.actor3);
					
					movie.latitude = coordinates.latitude
					movie.longitude = coordinates.longitude
					
					movies << movie
					
					gid = gid + 1
				}		
			}	
			//do not hit google service too hard
			sleep(600)	
		}	
		return movies
	}
}
