package sfmovie.services

import java.sql.SQLClientInfoException;

import sfmovie.controllers.beans.Movie;
import grails.transaction.Transactional
import groovy.sql.Sql;

//All movie relate CRUD operations
@Transactional
class MovieService {

	static dataSource
	
	/**
	 * this function is used to refresh the sf movie location database
	 * @param movies
	 * @return
	 * @throws Exception
	 */
	def refreshMovieDatabase(List<Movie> movies) throws Exception {
		deleteMovie()
		createMovie(movies)
	}
	
	/**
	 * 
	 * @return all movies in the movie database
	 * @throws Exception
	 */
	def getAllMovies() throws Exception {
		def sql = Sql.newInstance(dataSource)
		def allMovies = []
		def query = "select * from public.movie"
		sql.eachRow(query){
			Movie movie = new Movie()
			movie.id = it.id
			movie.title = it.title
			movie.releaseYear = it.release_year
			movie.longitude = it.longitude
			movie.latitude = it.latitude
			movie.funFacts = it.fun_facts
			movie.productionCompany = it.production_company
			movie.distributor = it.distributor
			movie.director = it.director
			movie.writer = it.writer
			movie.actors = []
			if (it.actor1!=null) {
				movie.actors.add(it.actor1)
			}
			if (it.actor2!=null) {
				movie.actors.add(it.actor2)
			}
			if (it.actor3!=null) {
				movie.actors.add(it.actor3)
			}
			movie.location = it.location
			
			allMovies << movie
		}
		return allMovies
	}
	
	/**
	 * 
	 * @return all movie filmed locations
	 * @throws Exception
	 */
	def getAllLocations() throws Exception {
		def sql =Sql.newInstance(dataSource)
		def movies = []
		def query = "select distinct location, longitude, latitude from public.movie order by location asc"
		sql.eachRow(query){
			Movie movie = new Movie()
			movie.location = it.location
			movie.longitude = it.longitude
			movie.latitude = it.latitude
			
			movies << movie
		}
		
		return movies	
	}
	
	/**
	 * this function is used to return movies filmed near a specific location in san francisco
	 * 
	 * @param longitude
	 * @param latitude
	 * @param totalResults
	 * @param nearbyMeters
	 * @return
	 * @throws Exception
	 */
	def getNearbyMovies(double longitude, double latitude, int totalResults, double nearbyMeters) throws Exception {
		def sql = Sql.newInstance(dataSource)
		def nearbyMovies = []
		def query = "select * from public.movie, (select ST_MakePoint(${longitude}, ${latitude})::geography as poi) as poi where ST_DWithin(geo, poi, ${nearbyMeters}) ORDER BY ST_Distance(geo, poi) asc limit ${totalResults}"
		sql.eachRow(query){
				Movie movie = new Movie()
				movie.id = it.id
				movie.title = it.title
				movie.releaseYear = it.release_year
				movie.longitude = it.longitude
				movie.latitude = it.latitude
				movie.funFacts = it.fun_facts
				movie.productionCompany = it.production_company
				movie.distributor = it.distributor
				movie.director = it.director
				movie.writer = it.writer
				movie.actors = []
				if (it.actor1!=null) {
					movie.actors.add(it.actor1)
				}
				if (it.actor2!=null) {
					movie.actors.add(it.actor2)
				}
				if (it.actor3!=null) {
					movie.actors.add(it.actor3)
				}
				movie.location = it.location
				
				nearbyMovies << movie
		}						
		return nearbyMovies
	}
	
	def deleteMovie() throws Exception{
		def sql = Sql.newInstance(dataSource)
		sql.execute('delete from public.movie')
	}
	
	/**
	 * insert movie data into movie database
	 * @param movies
	 * @return
	 */
	def createMovie(List<Movie> movies) throws Exception {
		def sql = Sql.newInstance(dataSource)
		sql.withBatch(200, 'insert into public.movie (id, title, release_year, longitude, latitude, fun_Facts, production_company, distributor, director, writer, actor1, actor2, actor3, location, geo) '
			+ 'values (?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,ST_MakePoint(?, ?))'){
			   preparedStatement ->
				   for (i in 0..movies.size()-1) {
					   preparedStatement.setInt(1, movies[i].id)
					   preparedStatement.setString(2, movies[i].title)
					   preparedStatement.setInt(3, movies[i].releaseYear)
					   preparedStatement.setDouble(4, movies[i].longitude)
					   preparedStatement.setDouble(5, movies[i].latitude)
					   preparedStatement.setString(6, movies[i].funFacts)
					   preparedStatement.setString(7, movies[i].productionCompany)
					   preparedStatement.setString(8,  movies[i].distributor)
					   preparedStatement.setString(9, movies[i].director)
					   preparedStatement.setString(10, movies[i].writer)
					   preparedStatement.setString(11, movies[i].actors[0])
					   preparedStatement.setString(12, movies[i].actors[1])
					   preparedStatement.setString(13, movies[i].actors[2])	
					   preparedStatement.setString(14, movies[i].location)
					   preparedStatement.setDouble(15, movies[i].longitude)
					   preparedStatement.setDouble(16, movies[i].latitude)
					   preparedStatement.addBatch()
				   }
		 }		
	}
}
