class UrlMappings {
	
	static mappings = {
		"/initSFMovieDB" (controller:"initializeData", action:"init")
		"/movies/nearby/$longitude/$latitude"(controller:"movie", action:"getNearbyMovies")
		"/movies"(controller:"movie", action:"getAllMovies")
		"/locations"(controller:"movie", action:"getAllLocations")		
	}
}
