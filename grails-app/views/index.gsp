<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Folder GUI</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/light/jquery-ui.min.css" rel="stylesheet">
    <link href="css/skin-lion/ui.fancytree.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
  </head>
  <body>
    
    <div>
        <p>User 1 folder structure</p>
        <div id="treeUser1"></div>
    </div>

    <br/>

    <div>
        <p>User 2 folder structure</p>
        <div id="treeUser2"></div>
    </div>

    <script type="text/javascript" src="js/lib.min.js"></script>
    <script type="text/javascript" src="js/foldershare.js"></script>
    <script type="text/javascript" src="js/jquery.fancytree-all.js"></script>
    <script>
        initTree();
    </script>
    
  </body>
</html>