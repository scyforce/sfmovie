[Movie SF] (https://moviesf.herokuapp.com/)
========
Movie SF is a simple web app which helps you find movies filmed near your current location within San Francisco.

---
![HOME](https://bytebucket.org/scyforce/sfmovie/raw/404ee0b3a8d7626d8972d5d4c9276dbcd01cfe66/web-app/img/home.png)
---
This app will load your current position and try to find movies filmed near your current location, or you can also search your desired location to see nearby movies and filter by certain criteria.

![Icon1](https://bytebucket.org/scyforce/sfmovie/raw/404ee0b3a8d7626d8972d5d4c9276dbcd01cfe66/web-app/img/multi.png)
means there are multiple movies filmed in the same location

---
Technical Specs

I use [grails](https://grails.org/) web framework to build the app. Backend is written using groovy and movie data is fetched from [SFGov](data.sfgov.org/resource/yitu-d5am.json) then geocoded by google geocoding service and stored in postgres.

Backend provides three services (JSON format)

 1 /movies 

   * display all movies information from database
   
 2 /locations

   * display all movies filmed location in san francisco
   
 3 /movies/nearby/longitude/latitude?radius=&numberOfResults= (e.g /movies/nearby/-122.41874/37.802139?radius=1&numberOfResults=40)
   
   * radius is in mile
   
   * display movies filmed near the specified (latitude, longitude).

Frontend is a one page application written in javascript, I use a couple opensource frameworks - jQuery, backbone, underscore, typeahead and bootstrap. All library files are in [lib.min.js](https://bitbucket.org/scyforce/sfmovie/src/6aa37daef490f27cfda30da184d1c0f599c93fbc/web-app/js/lib.min.js)

Code I write are in [controller](https://bitbucket.org/scyforce/sfmovie/src/404ee0b3a8d7626d8972d5d4c9276dbcd01cfe66/grails-app/controllers), [service](https://bitbucket.org/scyforce/sfmovie/src/404ee0b3a8d7626d8972d5d4c9276dbcd01cfe66/grails-app/services), [sfmovie.js](https://bitbucket.org/scyforce/sfmovie/src/404ee0b3a8d7626d8972d5d4c9276dbcd01cfe66/web-app/js/sfmovie.js), [core.css](https://bitbucket.org/scyforce/sfmovie/src/404ee0b3a8d7626d8972d5d4c9276dbcd01cfe66/web-app/css/core.css) and [index.html](https://bitbucket.org/scyforce/sfmovie/src/404ee0b3a8d7626d8972d5d4c9276dbcd01cfe66/web-app/index.html).

Some issues I found around the data - locations are missing in some movies, some addresses can not be geocoded and also google may geocode some address in wrong location.

Shuchun